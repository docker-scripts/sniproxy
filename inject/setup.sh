#!/bin/bash -x

test -s /etc/sniproxy.conf || {
    cp /etc/sniproxy.conf.sample /etc/sniproxy.conf ;
    service sniproxy restart ;
}
