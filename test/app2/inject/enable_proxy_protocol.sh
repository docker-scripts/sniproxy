#!/bin/bash -x

ip=$(hostname -I)
network=${ip%.*}.0/16

cat <<EOF > /etc/apache2/conf-available/proxy_protocol.conf
RemoteIPProxyProtocol On
RemoteIPTrustedProxy $network
RemoteIPHeader X-Forwarded-For
EOF

a2enconf proxy_protocol
a2enmod remoteip
systemctl restart apache2
