#!/bin/bash -x

source /host/settings.sh

# get a list of all non-example domains
for domain in $DOMAIN_NAMES; do
    [[ $domain =~ ^(.*\.)?example\.org$ ]] && continue
    [[ $domain =~ ^(.*\.)?example\.com$ ]] && continue
    [[ $domain =~ \.local$ ]] && continue
    domain_list+=" $domain"
done
[[ -n $domain_list ]] || exit

# get a certificate from letsencrypt
domain=$(echo $domain_list | cut -d' ' -f1)
certbot certonly --webroot \
        --non-interactive --agree-tos --keep-until-expiring \
        --expand --renew-with-new-domains --allow-subset-of-names \
        --webroot-path /var/www \
        --email admin@$domain \
        --domains $(echo $domain_list | tr ' ' , )

# update the configuration of apache2
cert_dir=/etc/letsencrypt/live/$domain
if [[ -f $cert_dir/cert.pem ]]; then
    sed -i /etc/apache2/sites-available/default-ssl.conf -r \
        -e "s|#?SSLCertificateFile.*|SSLCertificateFile      $cert_dir/cert.pem|" \
        -e "s|#?SSLCertificateKeyFile.*|SSLCertificateKeyFile   $cert_dir/privkey.pem|" \
        -e "s|#?SSLCertificateChainFile.*|SSLCertificateChainFile $cert_dir/chain.pem|"

    systemctl reload apache2
fi
