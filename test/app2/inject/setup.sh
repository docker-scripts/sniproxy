#!/bin/bash -x

source /host/settings.sh

main() {
    modify_frontpage
    enable_https
    setup_letsencrypt

    systemctl reload apache2
}

modify_frontpage() {
    # display the domain names on the front page 
    echo "$DOMAIN_NAMES" > /var/www/html/index.html
}

enable_https() {
    a2ensite default-ssl
    a2enmod ssl
    systemctl restart apache2
}

setup_letsencrypt() {
    # configure apache2 for letsencrypt
    mkdir -p /var/www/.well-known/acme-challenge/
    cat <<EOF > /etc/apache2/conf-available/letsencrypt.conf
Alias /.well-known/acme-challenge /var/www/.well-known/acme-challenge
<Directory /var/www/.well-known/acme-challenge>
    Options None
    AllowOverride None
    ForceType text/plain
</Directory>
EOF
    a2enconf letsencrypt
}

main "$@"
