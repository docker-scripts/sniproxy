#!/bin/bash -x

source /host/settings.sh

main() {
    modify_frontpage
    enable_https
    setup_letsencrypt

    systemctl reload nginx
}

modify_frontpage() {
    # display the domain names on the front page 
    echo "$DOMAIN_NAMES" > /var/www/html/index.html
}

enable_https() {
    sed -i /etc/nginx/sites-available/default \
        -e '/443/ s/# //' \
        -e '/snakeoil/ s/# //'
}

setup_letsencrypt() {
    cat <<EOF > /etc/nginx/snippets/letsencrypt.conf
location ^~ /.well-known/acme-challenge/ {
    allow all;
    default_type "text/plain";
    root /var/www/certbot;
}

location = /.well-known/acme-challenge/ {
    return 404;
}

ssl_certificate     /etc/ssl/certs/ssl-cert-snakeoil.pem;
ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;
EOF

    sed -i /etc/nginx/sites-available/default \
        -e 's/snakeoil\.conf/letsencrypt.conf/'

    mkdir -p /var/www/certbot
    chown www-data: /var/www/certbot
}

main "$@"
