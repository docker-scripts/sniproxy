#!/bin/bash -x

# If proxy_protocol is set for a port, then it is enabled
# for all the servers that listen to that port.

ip=$(hostname -I)
network=${ip%.*}.0/16

cat <<EOF > /etc/nginx/conf.d/proxy_protocol.conf
set_real_ip_from $network;
real_ip_header proxy_protocol;

server {
    listen 80 proxy_protocol;
    listen 443 proxy_protocol;
}
EOF

systemctl reload nginx
