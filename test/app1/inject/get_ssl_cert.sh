#!/bin/bash -x

source /host/settings.sh

# get a list of all non-example domains
for domain in $DOMAIN_NAMES; do
    [[ $domain =~ ^(.*\.)?example\.org$ ]] && continue
    [[ $domain =~ ^(.*\.)?example\.com$ ]] && continue
    [[ $domain =~ \.local$ ]] && continue
    domain_list+=" $domain"
done
[[ -n $domain_list ]] || exit

# get a certificate from letsencrypt
domain=$(echo $domain_list | cut -d' ' -f1)
certbot certonly --webroot \
        --non-interactive --agree-tos --keep-until-expiring \
        --expand --renew-with-new-domains --allow-subset-of-names \
        --webroot-path /var/www/certbot \
        --email admin@$domain \
        --domains $(echo $domain_list | tr ' ' , )

# update the configuration of nginx
cert_dir=/etc/letsencrypt/live/$domain
if [[ -f $cert_dir/fullchain.pem ]]; then
    sed -i /etc/nginx/snippets/letsencrypt.conf \
        -e '/ssl_certificate/d'
    cat <<EOF >> /etc/nginx/snippets/letsencrypt.conf
ssl_certificate     /etc/letsencrypt/live/$domain/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/$domain/privkey.pem;
EOF
    systemctl reload nginx
fi
