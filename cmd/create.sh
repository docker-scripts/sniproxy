rename_function cmd_create ds_cmd_create
cmd_create() {
    mkdir -p etc/
    touch etc/sniproxy.conf
    ds_cmd_create \
        --publish 80:80 --publish 443:443 \
        --mount type=bind,src=$(pwd)/etc/sniproxy.conf,dst=/etc/sniproxy.conf \
        "$@"    # accept additional options
}
