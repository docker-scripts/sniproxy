include(bookworm)

RUN <<EOF
  apt install --yes sniproxy
  cp /etc/sniproxy.conf /etc/sniproxy.conf.sample
  systemctl enable sniproxy
EOF