# sniproxy container

## Installation

  - First install `ds`: https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull sniproxy`

  - Create a directory for the container: `ds init sniproxy @sniproxy`

  - Make the container: `cd /var/ds/sniproxy/ ; ds make`

## Other commands

```
ds stop
ds start
ds shell
ds help
```
